This application is designed to provide a simple API interface to take personal information,
encrypt this and persist ths data to a backend repository.

mvp:

- The data should be encrypted

- Code should be unit tested

- The project should be built using MVN

- The project should be hosted on a GIT platform (e.g. GitHub or bitbucket)

nice to have:

- The end point should check for malicious code being passed into it

- Provide a method to decrypt the information

to run application mvn spring-boot:run

example service call data input
http://localhost:8080/datainput?firstName=rob&surname=pawley

example data retrieval
http://localhost:8080/dataretrieval?surname=pawley

For POC purposes a H2 in memory database is used
please navigate to http://localhost:8080/h2-console/
and change JDBC URL to jdbc:h2:mem:testdb