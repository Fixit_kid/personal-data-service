package uk.co.robertpaw.personaldata.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import uk.co.robertpaw.personaldata.helper.EncryptionHelper;
import uk.co.robertpaw.personaldata.model.Individual;
import uk.co.robertpaw.personaldata.repository.IndividualRepository;

import javax.crypto.Cipher;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataInputServiceTests {

    DataInputService dataInputService;

    @Mock
    IndividualRepository repository;

    @Mock
    EncryptionHelper encryptionHelper;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        dataInputService = new DataInputService(repository);
        dataInputService.setEncryptionHelper(encryptionHelper);
    }

    @Test
    public void testDataInput(){
        when(encryptionHelper.encryptionStringData("firstName", Cipher.ENCRYPT_MODE)).thenReturn("encFirst");
        when(encryptionHelper.encryptionStringData("surname", Cipher.ENCRYPT_MODE)).thenReturn("encSur");
        dataInputService.inputIndividual("firstName", "surname");
        verify(repository, times(1)).save(any(Individual.class));
    }

    @Test(expected = IllegalStateException.class)
    public void testExceptionThrown(){
        when(encryptionHelper.encryptionStringData("firstName", Cipher.ENCRYPT_MODE)).thenReturn("encFirst");
        when(encryptionHelper.encryptionStringData("surname", Cipher.ENCRYPT_MODE)).thenThrow(new IllegalStateException());
        dataInputService.inputIndividual("firstName", "surname");
        verify(repository, times(0)).save(any(Individual.class));
    }

}
