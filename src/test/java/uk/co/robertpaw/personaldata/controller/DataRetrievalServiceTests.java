package uk.co.robertpaw.personaldata.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import uk.co.robertpaw.personaldata.helper.EncryptionHelper;
import uk.co.robertpaw.personaldata.model.Individual;
import uk.co.robertpaw.personaldata.repository.IndividualRepository;

import javax.crypto.Cipher;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataRetrievalServiceTests {

    DataRetrievalService dataRetrievalService;

    @Mock
    IndividualRepository repository;

    @Mock
    EncryptionHelper encryptionHelper;

    List<Individual> resultsList;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        dataRetrievalService = new DataRetrievalService(repository);
        dataRetrievalService.setEncryptionHelper(encryptionHelper);
        Individual individual = new Individual();
        individual.setFirstName("encFirst");
        individual.setSurname("encSurname");
        resultsList = new ArrayList<Individual>();
        resultsList.add(individual);
    }

    @Test
    public void testDataRetrieval() {
        when(encryptionHelper.encryptionStringData("surname", Cipher.ENCRYPT_MODE)).thenReturn("encSur");
        when(repository.findBySurname("encSur")).thenReturn(resultsList);
        when(encryptionHelper.encryptionStringData("encFirst", Cipher.DECRYPT_MODE)).thenReturn("decFirst");
        when(encryptionHelper.encryptionStringData("encSurname", Cipher.DECRYPT_MODE)).thenReturn("decSur");

        List<Individual> individuals = dataRetrievalService.retrieveIndividual("surname");
        assert individuals.get(0).getFirstName().equals("decFirst");
        assert individuals.get(0).getSurname().equals("decSur");
    }

    @Test(expected = IllegalStateException.class)
    public void testExceptionThrown() {
        when(encryptionHelper.encryptionStringData("surname", Cipher.ENCRYPT_MODE)).thenThrow(new IllegalStateException());
        dataRetrievalService.retrieveIndividual("surname");

    }

}
