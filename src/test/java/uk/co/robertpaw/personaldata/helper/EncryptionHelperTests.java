package uk.co.robertpaw.personaldata.helper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EncryptionHelperTests {

    EncryptionHelper encryptionHelper;
    Cipher cipher;
    SecretKeySpec secretKeySpec;
    String target;
    String expected;


    @Before
    public void setup() throws NoSuchPaddingException, NoSuchAlgorithmException {
        MockitoAnnotations.initMocks(this);
        target = "Test";
        expected = "zftE5e5hXoWxGdKh0j2O7A==";
        cipher = Cipher.getInstance("AES");
        secretKeySpec = new SecretKeySpec("TestTestTestTest".getBytes(), "AES");
        encryptionHelper = new EncryptionHelper();
        encryptionHelper.setSecretKey(secretKeySpec);
        encryptionHelper.setCipher(cipher);
    }

    @Test
     public void testReturnEncryption() throws BadPaddingException, IllegalBlockSizeException {
        String result = encryptionHelper.encryptionStringData(target, Cipher.ENCRYPT_MODE);
        assert result.equals(expected);
    }

    @Test
    public void testReturnDecryption() throws BadPaddingException, IllegalBlockSizeException {
        String result = encryptionHelper.encryptionStringData(expected, Cipher.DECRYPT_MODE);
        assert result.equals(target);
    }

    @Test(expected = IllegalStateException.class)
    public void testThrownException(){
        secretKeySpec = new SecretKeySpec("toShort".getBytes(), "AES");
        encryptionHelper.setSecretKey(secretKeySpec);
        encryptionHelper.encryptionStringData(target, Cipher.ENCRYPT_MODE);
    }

}
