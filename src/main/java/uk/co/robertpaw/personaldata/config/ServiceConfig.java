package uk.co.robertpaw.personaldata.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import uk.co.robertpaw.personaldata.helper.EncryptionHelper;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;

@Configuration
public class ServiceConfig {

    @Autowired
    Environment env;

    @Bean
    public SecretKeySpec secretKey() {
        return new SecretKeySpec(env.getProperty("key.password").getBytes(), "AES");
    }

    @Bean
    public Cipher cipher() throws NoSuchPaddingException, NoSuchAlgorithmException {
        Cipher cipher = Cipher.getInstance("AES");
        return cipher;
    }

    @Bean
    public EncryptionHelper encryptionHelper(){
        return new EncryptionHelper();
    }
}
