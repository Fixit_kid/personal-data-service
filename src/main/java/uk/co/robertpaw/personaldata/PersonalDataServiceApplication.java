package uk.co.robertpaw.personaldata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import uk.co.robertpaw.personaldata.repository.IndividualRepository;

@SpringBootApplication
public class PersonalDataServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonalDataServiceApplication.class, args);
	}

}
