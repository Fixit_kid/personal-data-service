package uk.co.robertpaw.personaldata.helper;


import org.springframework.beans.factory.annotation.Autowired;
import sun.misc.BASE64Decoder;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class EncryptionHelper {

    @Autowired
    SecretKeySpec secretKey;

    @Autowired
    Cipher cipher;

    public String encryptionStringData(String target, Integer mode) {
        try {
            cipher.init(mode, secretKey);
            byte[] result;
            if (mode.equals(Cipher.DECRYPT_MODE)) {
                byte[] decordedValue = new BASE64Decoder().decodeBuffer(target);
                result = cipher.doFinal(decordedValue);
                return new String(result);
            } else {
                result = cipher.doFinal(target.getBytes());
                return Base64.getEncoder().encodeToString(result);
            }
        } catch (Exception e) {
            throw new IllegalStateException("unable to encrypt string");
        }
    }

    public void setSecretKey(SecretKeySpec secretKey) {
        this.secretKey = secretKey;
    }

    public void setCipher(Cipher cipher) {
        this.cipher = cipher;
    }
}
