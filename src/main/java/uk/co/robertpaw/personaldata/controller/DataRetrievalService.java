package uk.co.robertpaw.personaldata.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import uk.co.robertpaw.personaldata.helper.EncryptionHelper;
import uk.co.robertpaw.personaldata.model.Individual;
import uk.co.robertpaw.personaldata.repository.IndividualRepository;

import javax.crypto.Cipher;
import java.util.List;

@Controller
public class DataRetrievalService {

    @Autowired
    EncryptionHelper encryptionHelper;

    IndividualRepository individualRepository;

    public DataRetrievalService(IndividualRepository individualRepository){
        this.individualRepository = individualRepository;
    }

    @RequestMapping("/dataretrieval")
    @ResponseBody
    public List retrieveIndividual(@RequestParam String surname){
        String encSurname = encryptionHelper.encryptionStringData(surname, Cipher.ENCRYPT_MODE);
        List<Individual> resultsList = individualRepository.findBySurname(encSurname);
        for(Individual individual : resultsList){
            individual.setFirstName(
                    encryptionHelper.encryptionStringData(individual.getFirstName(), Cipher.DECRYPT_MODE));
            individual.setSurname(
                    encryptionHelper.encryptionStringData(individual.getSurname(), Cipher.DECRYPT_MODE));
        }
        return resultsList;
    }

    public void setEncryptionHelper(EncryptionHelper encryptionHelper) {
        this.encryptionHelper = encryptionHelper;
    }
}
