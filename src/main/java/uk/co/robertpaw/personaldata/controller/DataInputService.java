package uk.co.robertpaw.personaldata.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import uk.co.robertpaw.personaldata.helper.EncryptionHelper;
import uk.co.robertpaw.personaldata.model.Individual;
import uk.co.robertpaw.personaldata.repository.IndividualRepository;

import javax.crypto.Cipher;


@Controller
public class DataInputService {

    @Autowired
    EncryptionHelper encryptionHelper;

    IndividualRepository individualRepository;

    public DataInputService(IndividualRepository individualRepository){
        this.individualRepository = individualRepository;
    }

    @RequestMapping("/datainput")
    @ResponseBody
    public String inputIndividual(@RequestParam String firstName, @RequestParam String surname){
        Individual individual = new Individual();
        individual.setFirstName(encryptionHelper.encryptionStringData(firstName, Cipher.ENCRYPT_MODE));
        individual.setSurname(encryptionHelper.encryptionStringData(surname, Cipher.ENCRYPT_MODE));
        individualRepository.save(individual);
        return "Encrypted and persisted " + firstName + " " + surname;
    }

    public void setEncryptionHelper(EncryptionHelper encryptionHelper) {
        this.encryptionHelper = encryptionHelper;
    }
}
