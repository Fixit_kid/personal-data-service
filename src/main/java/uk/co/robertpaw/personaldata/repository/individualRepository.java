package uk.co.robertpaw.personaldata.repository;

import org.springframework.data.repository.CrudRepository;
import uk.co.robertpaw.personaldata.model.Individual;

import java.util.List;

public interface IndividualRepository extends CrudRepository<Individual, Long> {

    List<Individual> findBySurname(String surname);
}
